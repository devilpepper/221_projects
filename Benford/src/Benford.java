import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Jean Pena
 */

public class Benford {

	public static void main(String[] args){
		//frequency table size, variable to hold rounded percentage,
		//denominator, and the actual frequency table
		int tblSize = 10, rounded;
		double den = 0, d;
		int[] sig = new int[tblSize];
		
		//surrounding initializer in try/catch causes
		//"inFile may be uninitialized" ERROR... Should be a warning
		//initializing with null.
		Scanner inFile = null;
		
		//try to open data.txt. printStackTrace() provides
		//a sufficient error message if the file is not found.
		try {
			inFile = new Scanner(new FileInputStream("data.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*//experiment. Beat the clock and move data.txt within 10 seconds
		System.out.println("GO GO GO!");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		//it's like a map<int, int> in c++, except I explicitly gave it size 10
		//passes each variable to get1stSigFig to get the index to increment.
		
		//3 different methods of getting sigfigs
		//while(inFile.hasNext())sig[get1stSigFig2(inFile.nextDouble())]++;
		//while(inFile.hasNext())sig[get1stSigFig(inFile.nextDouble())]++;
		while(inFile.hasNext())sig[get1stSigFig(inFile.nextLine())]++;
		
		//done with the file
		inFile.close();
		
		//add up all the frequencies to get the denominator(ignoring 0s)
		for(int i=1; i<tblSize; i++) den += sig[i];
		
		//print out each histogram bin vertically
		for(int i=1; i<tblSize; i++)
		{
			//get d as a percent
			d = sig[i]*100/den;
			//round d to the nearest integer
			rounded = (int)Math.round(d);
			//i (ith % 3 decimal places)\t: 
			System.out.printf("%d (%.3f%%)\t: *", i, d);
			//histogram made of asterisks
			for(int j=0; j<rounded; j++)System.out.print('*');
			//next line for the (i+1)th row if needed
			System.out.println();
		}
	}
	
	public static int get1stSigFig(String d) //regex
	{
		Matcher m = Pattern.compile("[1-9]").matcher(d);
		if(m.find()) return Integer.parseInt(m.group());
		return 0;
	}
	public static int get1stSigFig(double d) //recursive
	{
		if(d >= 10) return get1stSigFig2(d/10);
		else if(d < 1) return get1stSigFig2(d*10);
		return (int) d;
	}
	public static int get1stSigFig2(double d)
	{
		//convert double to string
		String s = Double.toString(d);
		int i = 0;
		//from left to right search for 1st significant figure
		while(i<s.length() && (s.charAt(i) == '0' || s.charAt(i) == '-' || s.charAt(i) == '.')) i++;
		//return first significant figure as int
		return (i < s.length() ? Character.getNumericValue(s.charAt(i)) : 0);
	}
}